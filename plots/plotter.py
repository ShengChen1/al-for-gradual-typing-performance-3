import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
from utils.tools import setMarkers, addErrorBar
import pickle

DATASET = 'Meteor'

PATH = "D:/PyProject/Code-Config-Analyst/"
rcParams['font.family'] = 'Times New Roman'
rcParams['font.size'] = 25
rcParams["legend.columnspacing"] = 0.1
# plt.rc('xtick', labelsize=24)
# plt.rc('ytick', labelsize=24)
# plt.rcParams['text.usetex'] = True
colors = ['#3282b8', '#fe9801']
markerSize = 10


with open(PATH + DATASET + '.result', "rb") as Raytrace:
        Result = pickle.load(Raytrace)





x = Result['Sample_Size']
markers_on = setMarkers(x, 20, x[0])
y_QOB = np.mean(np.array(Result['QBC_Loss']), axis=0)
y_Random = np.mean(np.array(Result['Random_Loss']), axis=0)

fig = plt.figure(figsize=(12, 8))
ax = fig.add_subplot(1, 1, 1)
# plt.subplots_adjust(wspace=0, hspace=0, left=0.1, bottom=0.08, right=0.99, top=0.95)

ax.plot(x, y_QOB, color=colors[0],
        marker='o', markevery=markers_on,
        markersize=markerSize, linewidth=3.0,
        label='QBC')

ax.plot(x, y_Random, color=colors[1],
        marker='^', markevery=markers_on,
        markersize=markerSize, linewidth=3.0,
        label='Random')


# Baselines
if DATASET == 'RayTrace':
        MLP_Batch, SVM_Batch = 2.01 * np.ones(len(x)), 2.55 * np.ones(len(x))
        SIZE = 2047
elif DATASET == 'Meteor':
        MLP_Batch, SVM_Batch = .32 * np.ones(len(x)), 1.05 * np.ones(len(x))
        SIZE = 1024
elif DATASET == "NBody":
        MLP_Batch, SVM_Batch = .5 * np.ones(len(x)), 2.22 * np.ones(len(x))
        SIZE = 1024


ax.plot(x, SVM_Batch, color= '#42e6a4', linestyle='--', linewidth=3.0, label='SVM (Passive)')
ax.plot(x, MLP_Batch, color= 'r', linestyle='--', linewidth=3.0, label='MLP (Passive)')




addErrorBar(x, markers_on, Result['QBC_Loss'], ax, '#347474')
addErrorBar(x, markers_on, Result['Random_Loss'], ax, '#f7be16')

ax.set_xlabel('# of Instances')
ax.set_ylabel('RMSE (in Seconds)')
plt.title(DATASET + ', Size='+str(SIZE))
plt.grid(True)
plt.legend()
plt.show()
fig.savefig(DATASET + "_result.pdf", bbox_inches='tight')