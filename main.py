import numpy as np
from scipy import stats
from dataset import data_loader as loader
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.svm import SVR, SVC
from sklearn.neural_network import MLPClassifier, MLPRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from alipy.experiment.al_experiment import AlExperiment
from methods.max_uncertain_QBC import MaxUncertainQBC as QBC
from methods.random_sampling import RandomSampling
import pickle
import time

# Seed = 0
# np.random.seed(Seed)
NUM_QUARY = 100
HELDOUT_RATIO = 0.1
START_SAMPLE_SIZE = 0.1
COMMITTEE_SIZE = 30
CROSS_VALIDATION = 10
DATASET = 'Meteor'


if DATASET == 'Meteor':
    X, y = loader.meteor()
elif DATASET == 'RayTrace':
    X, y = loader.raytrace()
elif DATASET == "NBody":
    X, y = loader.nbody()


Sample_Size, Random_Loss, QBC_Loss = [], [], []
Result = {'Sample_Size': [],
          'Random_Loss': [],
          'QBC_Loss':[]}

start_time = time.time()
for cv in range(CROSS_VALIDATION):
    print('Cross Validation Round #' + str(cv) + '... ...')
    Committee = []
    for n_learners in range(COMMITTEE_SIZE):
        Committee.append(MLPRegressor(max_iter=500))

    AL = QBC(X, y, StartRatio=START_SAMPLE_SIZE,
             n_Query= NUM_QUARY,
             TestSize=HELDOUT_RATIO)
    AL.train(Committee)
    QBC_Loss.append(AL.loss)

    Random_AL = RandomSampling(X, y, StartRatio=START_SAMPLE_SIZE,
                               n_Query=NUM_QUARY,
                               TestSize=HELDOUT_RATIO)
    learner = MLPRegressor(max_iter=500)
    Random_AL.train(learner)
    Random_Loss.append(Random_AL.loss)

print('runtime: '+ str(time.time() - start_time))

Result['Sample_Size'], Result['Random_Loss'], Result['QBC_Loss'] = AL.sampleSize, Random_Loss, QBC_Loss
with open(DATASET + '.result', 'wb') as f:
    pickle.dump(Result, f)
