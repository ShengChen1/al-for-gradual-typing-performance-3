import math
import numpy as np

def setMarkers(X_axis, nMarkers, start):
    markerLoc = []
    markerNum = nMarkers
    markerSec = math.floor(len(X_axis) / markerNum)
    for i in range(100):
        mLoc = i * markerSec
        if mLoc < len(X_axis):
            markerLoc.append(mLoc)
        else: break
    # markerLoc[0] = start
    # if markerLoc[1] - markerLoc[0] <=  markerSec:
    #     markerLoc[1] = markerLoc[0]
    return markerLoc


def addErrorBar(x, marker_list, y_list, ax, color):
    errorbar_baseline_mid, yerr, marker_loc = [], [], []
    error_mean = np.mean(np.array(y_list), axis=0)
    error_std = np.std(np.array(y_list), axis=0)
    for i in marker_list:
        marker_loc.append(x[i])
        errorbar_baseline_mid.append(error_mean[i])
        yerr.append(error_std[i])
    # yerr = .15 * np.ones(len(marker_list))
    ax.errorbar(marker_loc, errorbar_baseline_mid,
                yerr=yerr,fmt='',color=color, ls='',
                capsize=2, elinewidth=2, markeredgewidth=2)

